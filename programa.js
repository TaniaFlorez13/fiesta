//Invitados
let i01;
i01 = new Invitado;
i01.nombre=(Boris);
i01.apellido=(Quintero)
i01.edad=(19);
i01.asistencia();
i01.tomar(b02);
i01.consumir(c03);
i01.bailar(m01);
i01.hablar(i03,i04);

let i02;
i02 = new Invitado;
i02.nombre=(Jose);
i02.apellido=(Rojas);
i02.edad=(20);
i02.asistencia();
i02.escuchar(m03);
i02.comer(c06);

let i03;
i03 = new Invitado;
i03.nombre=(Diana);
i03.apellido=(Flórez)
i03.edad=(24);
i03.asistencia();
i03.tomar(b01);
i03.consumir(c04);
i03.bailar(m02);
i03.hablar(i01, i04);

let i04;
i04 = new Invitado;
i04.nombre=(Cristian);
i04.apellido=(Sánchez);
i04.edad=(19);
i04.asistencia();
i04.comer(c05);
i04.escuchar(m01);
i04.tomar(b03);
i04.hablar(i01,i03);

let i05;
i05 = new Invitado;
i05.nombre=(Angie);
i05.apellido=(Lopez)
i05.edad=(20);
i05.asistencia();
i05.tomar(b02);
i05.consumir(c02);
i05.bailar(m04);
i05.hablar(i08);

let i06;
i06 = new Invitado;
i06.nombre=(Jose);
i06.apellido=(Fajardo)
i06.edad=(21);
i06.asistencia();
i06.tomar(b02);
i06.consumir(c01);
i06.bailar(m05);
i06.hablar(i07);

let i07;
i07 = new Invitado;
i07.nombre=(Catalina);
i07.apellido=(Sánchez);
i07.edad=(20);
i07.asistencia();
i07.comer(c04);
i07.beber(b01);
i07.escuchar(m01);
i07.hablar(i06);

let i08;
i08 = new Invitado;
i08.nombre=(Kathy);
i08.apellido=(Díaz);
i08.edad=(22);
i08.asistencia();
i08.comer(c05);
i08.beber(b03);
i08.escuchar(m03);
i08.hablar(i05);

// bebidas
let b01;
b01 = new Bebida;
b01.identificar(Smirnoff);
b01.sabor(Lulo);
b01.alcohol('25%');
b01.beber();

let b02;
b02 = new Bebida;
b02.identificar(Aguardiente);
b02.sabor(DobleAnis);
b02.alcohol('29%');
b02.beber();

let b03;
b03 = new Bebida;
b03.identificar(Agua);
b03.sabor(Sinsabor);
b03.alcohol('0%');
b03.beber();

let b04;
b04 = new Bebida;
b04.identificar(CremadeWhisky);
b04.sabor(Café);
b04.alcohol('14.66%');
b04.beber();


//comida
let c01;
c01 = new Comida;
c01.identificar(AsadoHuilense);
c01.consumir();

let c02;
c02 = new Comida;
c02.identificar(PerroCaliente);
c02.consumir();

let c03;
c03 = new Comida;
c03.identificar(Sandwich);
c03.consumir();

let c04;
c04 = new Comida;
c04.identificar(Hamburguesa);
c04.consumir();

let c05;
c05 = new Comida;
c05.identificar(PapasFritas);
c05.consumir();

let c06;
c06 = new Comida;
c06.identificar(TodoRico);
c06.consumir();

//musica
let m01;
m01 = new Musica;
m01.genero(Vallenato);
m01.bailar();
m01.escuchar();

let m02;
m02 = new Musica;
m02.genero(Salsa);
m02.bailar();
m02.escuchar();

let m03;
m03 = new Musica;
m03.genero(Popular);
m03.bailar();
m03.escuchar();

let m04;
m04 = new Musica;
m04.genero(Merengue);
m04.bailar();
m04.escuchar();

let m05;
m05 = new Musica;
m05.genero(Reggaeton);
m05.bailar();
m05.escuchar();